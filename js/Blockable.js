class Blockable extends PIXI.Sprite {
    constructor(x, y, width = 1, height = 1){
        super();
        // this.texture = PIXI.Texture.WHITE;
        // this.alpha = 0.5;
        this.block_state = 'blocking';
        this.x = x*128;
        this.y = y*128;
        this.width = width*128;
        this.height = height*128;
    }
}

export { Blockable }