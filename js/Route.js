import { Blockable } from "./Blockable.js";
import { Flower } from "./Flower.js";
import { Grass } from "./Grass.js";

const assets = {
    Flower, Grass
};

// const config = [
//     {
//         name: 'Route 1',
//         x: -6*128,
//         y: (9-40)*128,
//         background: {},
//         foreground: {},
//         blockers: [],
//         animatables: []
//     }
// ];

class Route extends PIXI.Sprite {
    constructor(configuration, layer){
        super();
        this.parentLayer = layer;
        this.zOrder = 0;
        this.name = configuration.name;
        this.x = configuration.x;
        this.y = configuration.y;
        
        this.setupBackground(configuration.background);
        this.setupForeground(configuration.foreground);
        this.setupBlockers(configuration.blockers);
        this.setupAnimatables(configuration.animatables);
    }

    setupBackground(background){
        this.texture = background.texture;
    }

    setupForeground(foreground){
        let f = new PIXI.Sprite(foreground.texture);
            f.parentLayer = this.parentLayer;
            f.zOrder = 2;
            f.x = 0;
            f.y = 0;

        this.addChild(f);
    }

    setupBlockers(blockers){
        for(let blocker of blockers){
           this.addChild(
                new Blockable(blocker.x, blocker.y, blocker.width, blocker.height)
           );
        }
    }

    setupAnimatables(animatables){
        for(let animatable of animatables){
            this.addChild(
                new assets[animatable.classname](animatable.x, animatable.y, this.parentLayer, animatable.textures)
            );
        }
    }
}

export { Route }