import { createAnimationFactory, Animatable } from "./Animation.js"

class Grass extends Animatable {
    constructor(x, y, layer, textures = []){
        super();

        this.textures = textures;

        this.texture = this.textures[0];

        this.x = x*128;
        this.y = y*128;

        this.animatable = true;
        
        this.hasPlayed = false;
        
        let grassAnimation = createAnimationFactory().createInstance('decorations', 'grass');
            grassAnimation.loop = false;
            grassAnimation.onComplete = _ => {
                this.children[0].gotoAndStop(0);
            };
            grassAnimation.parentLayer = layer;
            grassAnimation.zOrder = 3;

        this.addChild(grassAnimation)
    }

    play(){
        this.hasPlayed = true;

        this.setTexture(this.textures[1]);

        this.children[0].visible = true;
        this.children[0].gotoAndPlay(0);
    }

    stop(){
        this.children[0].visible = false;
        this.children[0].stop();

        this.hasPlayed = false;

        this.setTexture(this.textures[0]);
    }

    setTexture(texture){
        this.texture = texture;
    }
    
}

export { Grass }