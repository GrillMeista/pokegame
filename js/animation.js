var instance = undefined

class Animatable extends PIXI.Sprite {
    constructor(x, y, layer, textures = []){
        super();
        this.x = x;
        this.y = y;
        this.parentLayer = layer;
        this.animatable = true;
    }

    play(){}

    stop(){}
}

class AnimationFactory {
    constructor(config){
        this.animations = config.data;
    }

    createInstance(namespace, name){
        let animationData =  this.animations[namespace].find( p => p.name == name );
        let animation = new PIXI.AnimatedSprite.fromFrames(animationData.textures);
            animation.name = animationData.name;
            animation.animationSpeed = animationData.speed;
            animation.visible = false;

        return animation;
    }

    createFromNamespace(namespace){
        let animation, animations = [];

        for(let a of this.animations[namespace]){
            animation = new PIXI.AnimatedSprite.fromFrames(a.textures)
            animation.name = a.name;
            animation.animationSpeed = a.speed;
            animation.visible = false;

            animations.push(animation);
        }

        return animations;
    }
}

function createAnimationFactory(config = undefined){
    if(instance == undefined){
        instance = new AnimationFactory(config);
    }

    return instance;
}

export {AnimationFactory, createAnimationFactory, Animatable}