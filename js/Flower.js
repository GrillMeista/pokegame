import { createAnimationFactory, Animatable } from "./Animation.js"

class Flower extends Animatable {
    constructor(x, y, layer, textures = []){
        super();

        this.animatable = false;

        this.parentLayer = layer; 
        
        this.zOrder = 1;

        this.x = x*128;
        this.y = y*128;

        let flowerAnimation = createAnimationFactory().createInstance('decorations', 'red_flower');
            flowerAnimation.visible = true;
            flowerAnimation.play();
        
        this.addChild(flowerAnimation);
    }
}

export { Flower }