const ACTION = {
    PRESS_KEY: 'press',
    RELEASE_KEY: 'release' 
}

class KeyHandler {
    constructor(){
        this.listeners = [];
    }

    assign(key, action, callback){
        let listener = this.getListener();
        
        if(listener == undefined){
            listener = new KeyListener(key).register();
            listener[action] = callback;

            this.listeners.push(listener);

        }else{
            listener[action] = callback;
        }
    }

    getListener(key){
        for(let i = 0; i < this.listeners.length; i++){
            if(this.listeners[i].value == key){
                return this.listeners[i];
            }
        }

        return undefined;
    }
}

class KeyListener {
    constructor(value){
        this.key = new Key(value);
    }

    register(){
        let downListener = this.key.downHandler.bind(this.key);
        let upListener = this.key.upHandler.bind(this.key);

        window.addEventListener("keydown", downListener, false);
        window.addEventListener("keyup", upListener, false);

        this.key.unsubscribe = () => {
            window.removeEventListener("keydown", downListener);
            window.removeEventListener("keyup", upListener);
        };
        
        return this.key;
    }
}

class Key {
    constructor(value){
        this.value = value;
        this.isDown = false;
        this.isUp = true;
        this.press = undefined;
        this.release = undefined;
    }

    downHandler(event){
        if (event.key === this.value) {
            if (this.isUp && this.press) this.press();
            this.isDown = true;
            this.isUp = false;
            event.preventDefault();
          }
    }

    upHandler(event){
        if (event.key === this.value) {
            if (this.isDown && this.release) this.release();
            this.isDown = false;
            this.isUp = true;
            event.preventDefault();
        }
    }
}

export { Key, KeyListener, KeyHandler, ACTION }