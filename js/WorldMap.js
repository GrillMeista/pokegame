import { Route } from "./Route.js"

class WorldMap extends PIXI.Container {
    constructor(configuration, layer){
        super();

        this.sortableChildren = true;

        this.parentLayer = layer;
        this.zOrder = 0;

        this.x = -768;
        this.y = -3968;

        this.setupRoutes(configuration.routes);
    }

    setupRoutes(routes){
        for(let route of routes){
            this.addChild( new Route(route, this.parentLayer) );
        }
    }

    getBlockers(){
        let blockers = [];

        for(let i = 0; i < this.children.length; i++){
            this.children[i].children
                .filter( e => e.block_state == 'blocking' )
                .forEach( e => blockers.push({
                    x: e.x + this.children[i].x, 
                    y: e.y + this.children[i].y, 
                    width: e.width, 
                    height: e.height
                }))
        }

        return blockers;
    }

    getAnimatables(){
        let animatables = [];

        for(let i = 0; i < this.children.length; i++){
            this.children[i].children
                    .filter( e => e.animatable == true )
                    .forEach( e => animatables.push(e))
        }

        return animatables;
    }
}

export { WorldMap }