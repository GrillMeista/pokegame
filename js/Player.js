import { createAnimationFactory } from "./Animation.js"
import { createStaticMovementController } from "./StaticMovementController.js"
import { ACTION } from "./Keyhandler.js"

class PlayerController {
    constructor(app){
        this.player = new Player([
            {name: 'up', texture: app.loader.resources['data/sprites/male/images/up.png'].texture},
            {name: 'down', texture: app.loader.resources['data/sprites/male/images/down.png'].texture},
            {name: 'left', texture: app.loader.resources['data/sprites/male/images/left.png'].texture},
            {name: 'right', texture: app.loader.resources['data/sprites/male/images/right.png'].texture}
        ], app.layer.world);

        app.stage.addChild(this.player);

        this.initKeylistener(app.keyHandler, app.current_stage);
    }
    
    initKeylistener(keyHandler, current_stage){
        let listeners = [
            {key: 'ArrowUp', value: 'up'},
            {key: 'ArrowDown', value: 'down'},
            {key: 'ArrowLeft', value: 'left'},
            {key: 'ArrowRight', value: 'right'},
            {key: 'Shift', value: ''}
        ];
    
        let shift = false;
    
        listeners
            .filter( l => l.key.includes('Arrow'))
            .forEach( l => {
                keyHandler.assign(l.key, ACTION.PRESS_KEY, _ => {
                        if(current_stage == 'open_world'){
                            this.player.move(l.value, shift);
                        }
                    });
    
                keyHandler.assign(l.key, ACTION.RELEASE_KEY, _ => {
                        if(current_stage == 'open_world'){
                            this.player.release(l.value);
                        }
                    });
            });
        
        keyHandler.assign('Shift', ACTION.PRESS_KEY, _ => {
            shift = true;
    
            listeners
                .filter( l => l.key.includes('Arrow'))
                .forEach( l => {
                    if(current_stage == 'open_world'){
                        if(keyHandler.getListener(l.key).isDown){
                            this.player.move(l.value, shift);
                        }
                    }
                });
        });
        
        keyHandler.assign('Shift', ACTION.RELEASE_KEY, _ => {
            shift = false;
    
            listeners
                .filter( l => l.key.includes('Arrow'))
                .forEach( l => {
                    if(current_stage == 'open_world'){
                        if(keyHandler.getListener(l.key).isDown){
                            this.player.move(l.value, shift);
                        }
                    }
                });
        });
    }
}

class Player extends PIXI.Sprite {
    constructor(textures = [], layer){
        super();
        
        this.x = 7*128;
        this.y = 3*128;

        this.velocity = {x: 0, y: 0};

        this.parentLayer = layer;

        this.zOrder = 1;

        this.textures = textures;

        this.movementController = createStaticMovementController();

        this.speed = 0;
        this.velocities = [];
        
        this.lastDirection = 'up';
        this.preLastDirection = 'up';

        this.init();

        this.setSpeed(8);

        this.setTexture('up');
    }

    init(){
        createAnimationFactory()
            .createFromNamespace('player')
            .forEach( animation => this.addChild(animation));
    }

    move(direction, running = false){
        this.texture = undefined;

        this.preLastDirection = this.lastDirection;
        this.lastDirection = direction;

        this.stopAnimations();

        let animation = undefined;

        if(running){
            animation = this.children.find( animation => animation.name == 'run_' + direction);
            this.setSpeed(16);

        }else{
            animation = this.children.find( animation => animation.name == 'walk_' + direction);
            this.setSpeed(8);

        }

        animation.visible = true;
        animation.play();

        for(let i = 0; i < this.velocities.length; i++){
            if(this.velocities[i].direction == direction){
                this.velocity.x = this.velocities[i].velocity.x;
                this.velocity.y = this.velocities[i].velocity.y;
            }
        }

        this.movementController.add({
            x: this.velocity.x,
            y: this.velocity.y,
            caller: this,
            stop: false,
            animation: {
                animation: animation,
                isPlaying: false
            }
        });
    }
    
    release(direction){

        if(direction == this.lastDirection){
            this.stopAnimations();

            this.velocity.x = 0;
            this.velocity.y = 0;

            this.setTexture(this.lastDirection)

            this.pressedDirections = [];

            this.movementController.reduce();
        }
        
    }

    playAnimation(name){
        this.stopAnimations();

        this.texture = undefined;

        let animation = this.children.find( animation => animation.name == name);
            animation.visible = true;
            animation.play();
    }

    stopAnimations(){
        this.children.forEach( animation => {
            animation.stop();
            animation.visible = false;
        });
    }

    stopAnimationsPlayed(){
        this.children.forEach( animation => {
            animation.stop();
            animation.visible = false;
        });

        this.setTexture(this.lastDirection);
    }

    setSpeed(speed){
        this.speed = speed;

        this.setVelocities([
            {direction: 'up', velocity: {x: 0, y: -speed}},
            {direction: 'down', velocity: {x: 0, y: speed}},
            {direction: 'left', velocity: {x: -speed, y: 0}},
            {direction: 'right', velocity: {x: speed, y: 0}}
        ]);
    }

    setVelocities(velocities){
        this.velocities = velocities;
    }

    setTexture(texture){
        this.texture = this.textures.find( t => t.name == texture).texture;
    }
}

export { Player, PlayerController }