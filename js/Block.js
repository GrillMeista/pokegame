class Block extends PIXI.Sprite {
    constructor(texture, layer){
        super(texture);

        this.parentLayer = layer;
        this.zOrder = 1;

        this.block_state = 'blocking';
    }
}

export { Block }