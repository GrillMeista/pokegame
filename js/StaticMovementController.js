var staticMovementController = undefined;

class StaticMovementController {
    constructor(moveObject, animatables, blockings){
        this.moveObject = moveObject;
        this.animatables = animatables;
        this.blockings = blockings;

        console.log(animatables, blockings);

        this.positions = [];
        this.counter = {
            x: 0, y: 0
        };
        this.moveBlock = {
            x: 0,
            y: 0
        };
        this.x = 0;
        this.y = 0;
        this.break = false;
    }

    add(position){
        for(let i = 0; i < this.positions.length; i++){
            if(position.x == this.positions[i].x && position.y == this.positions[i].y){
                return;
            }
        }

        if(this.positions.length > 0){
            this.positions[this.positions.length-1].stop = true;
        }

        this.positions.push(position);
    }

    reduce(){
        if(this.positions.length > 0){
            this.positions[this.positions.length-1].stop = true;
        }
    }

    move(delta){
        if(this.positions.length > 0){
            this.x = this.y = 0;

            if(this.positions[0].x != 0){
                this.x = this.positions[0].x * delta;
            }

            if(this.positions[0].y != 0){
                this.y = this.positions[0].y * delta;
            }

            let norm = this.normalize();

            this.counter.x += norm.x;
            this.counter.y += norm.y;

            if( Math.abs(this.counter.x) >= 128 ){
                if(Math.abs(this.counter.x) > 128){
                    this.moveMap((this.counter.x/Math.abs(this.counter.x)) * (128-Math.abs(this.counter.x)), 0);
                }else{
                    this.moveMap(norm.x, 0);
                }

                if(this.moveObject.x % 128 != 0){
                    this.moveObject.x = Math.round(this.moveObject.x / 128) * 128;
                }

                this.counter.x = 0;
                this.counter.y = 0;

                if(this.positions[0].stop == true){
                    this.positions.shift();
                }

                return;
            }

            if( Math.abs(this.counter.y) >= 128 ){
                if(Math.abs(this.counter.y) > 128){
                    this.moveMap(
                            0, (this.counter.y/Math.abs(this.counter.y)) * (128-Math.abs(this.counter.y))
                        );
                }else{
                    this.moveMap(0, norm.y);
                }

                if(this.moveObject.y % 128 != 0){
                    this.moveObject.y = Math.round(this.moveObject.y / 128) * 128;
                }

                this.counter.x = 0;
                this.counter.y = 0;

                if(this.positions[0].stop == true){
                    this.positions.shift();
                }

                return;
            }

            this.moveMap(norm.x, norm.y);
        }
    }

    moveMap(x, y){
        if(!this.isBlocked(x, y)){
            this.moveInBounds(x, y);
        }
    }

    normalize(){
        let output = {x: 0, y: 0};

        this.moveBlock.x += this.x;
        this.moveBlock.y += this.y;

        if(
            Math.abs(this.moveBlock.y) >= this.positions[0].caller.speed || 
            Math.abs(this.moveBlock.x) >= this.positions[0].caller.speed
        ){
        
            if(Math.abs(this.moveBlock.x) >= this.positions[0].caller.speed){
                output.x = this.moveBlock.x/Math.abs(this.moveBlock.x) * this.positions[0].caller.speed;
                output.y = 0;
            }
    
            if(Math.abs(this.moveBlock.y) >= this.positions[0].caller.speed){
                output.y = this.moveBlock.y/Math.abs(this.moveBlock.y) * this.positions[0].caller.speed;
                output.x = 0;
            }
            
            this.moveBlock.x = 0;
            this.moveBlock.y = 0;
        }

        return output;
    }

    isBlocked(x, y){
        let position = undefined;

        for(let j = 0; j < this.animatables.length; j++){
            position = {
                x: this.animatables[j].x + this.animatables[j].parent.x,
                y: this.animatables[j].y + this.animatables[j].parent.y
            };

            if(
                (this.moveObject.y-y) + position.y - this.animatables[j].height < this.positions[0].caller.y + 128 &&
                (this.moveObject.x-x) + position.x + this.animatables[j].width > this.positions[0].caller.x &&
                (this.moveObject.x-x) + position.x < this.positions[0].caller.x + 128 &&
                (this.moveObject.y-y) + position.y > this.positions[0].caller.y
            ){
                if(!this.animatables[j].hasPlayed){
                    this.animatables[j].play();
                }
            }else{
                this.animatables[j].stop();
            }
        }

        for(let i = 0; i < this.blockings.length; i++){
            if(
                (this.moveObject.x-x) + this.blockings[i].x < this.positions[0].caller.x + 128 &&
                (this.moveObject.x-x) + this.blockings[i].x + this.blockings[i].width > this.positions[0].caller.x &&
                (this.moveObject.y-y) + this.blockings[i].y < this.positions[0].caller.y + 256 &&
                (this.moveObject.y-y) + this.blockings[i].y + this.blockings[i].height > this.positions[0].caller.y + 128
            ){
                return true;
            }
        }

        return false;
    }

    moveInBounds(x, y){
        if(
            this.moveObject.x-x <= Math.floor(window.innerWidth/2) - 64 && 
            this.moveObject.x-x >= (-this.moveObject.width + Math.floor(window.innerWidth/2) + 64)
            // this.moveObject.x-x >= (-this.moveObject.texture.width + Math.floor(window.innerWidth/2) + 64)
            // object.x-x >= (-map.tileset.width + Math.floor(window.innerWidth/2) + 64)
        ){
            this.moveObject.x -= x;
        }else{
            this.moveObject.x = Math.round(this.moveObject.x/128)*128;
        }

        if(
            this.moveObject.y-y <= Math.floor(window.innerHeight/2) - 128 &&
            this.moveObject.y-y >= (-this.moveObject.height + Math.floor(window.innerHeight/2) + 128)
            // this.moveObject.y-y >= (-this.moveObject.texture.height + Math.floor(window.innerHeight/2) + 128)
            // object.y-y >= (-map.tileset.height + Math.floor(window.innerHeight/2) + 128)
        ){
            this.moveObject.y -= y;
        }else{
            // map.y = Math.round(map.y/128)*128;
            // map.y = 128 + Math.round(map.y/128)*128;

            this.moveObject.y = this.moveObject.y/Math.abs(this.moveObject.y) * 128 - Math.round(this.moveObject.y/128)*128;
        }
    }
}

export function createStaticMovementController(moveObject = undefined, animatables = undefined, blockings = undefined){
    if(staticMovementController == undefined){
        staticMovementController = new StaticMovementController(moveObject, animatables, blockings);
    }
    
    return staticMovementController;
}