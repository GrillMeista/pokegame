import { KeyHandler } from "./Keyhandler.js"

class Application extends PIXI.Application {
    constructor(){
        super();
        this.renderer.view.style.position = "absolute";
        this.renderer.view.style.display = "block";
        this.renderer.autoResize = true;
        this.renderer.resize(window.innerWidth, window.innerHeight);
        this.ticker.maxFPS = 144;

        this.stage = new PIXI.display.Stage();
        this.stage.sortableChildren = true;
        
        this.layer = {};
        this.keyHandler = new KeyHandler();

        this.current_stage = 'open_world';

        this.addLayer('world');
        this.addLayer('combat');

        this.loader = PIXI.Loader.shared;
        this.setupLoader();
    }

    addLayer(name){
        this.layer[name] = new PIXI.display.Layer();
        this.layer[name].group.enableSort = true;

        this.stage.addChild(this.layer[name]);
    }

    setupLoader(){
        this.loader.add([
            'data/route_1_background.png',
            'data/tileset_128x128.png',
            'data/route_1_tree_peaks.png',
            'data/pallet_town.png',
            'data/pallet_town_foreground.png',
            'data/battle_background_2.png',
            'data/config/sourcemap.json',
            'data/config/animations.json',
            'data/config/combat.json',
            'data/config/routes.json',
            'data/sprites/male/images/up.png',
            'data/sprites/male/images/down.png',
            'data/sprites/male/images/left.png',
            'data/sprites/male/images/right.png',
            'data/rock.png',
            'data/assets/grass/default.png',
            'data/assets/grass/lowered.png'
        ])
    }
}

export { Application }