import { createAnimationFactory } from "./Animation.js"

class CombatPokemon extends PIXI.Sprite {
    constructor(name, owner = ''){
        super();

        let direction = '';

        if(owner == 'player'){
            direction = '_back';
        }

        let animation = createAnimationFactory()
            .createInstance('pokemon', name+direction);

        animation.visible = true;
        animation.play();
        
        this.addChild(animation);
    }
}

class CombatInterface extends PIXI.Container {
    constructor(configuration){
        super();

        this.configuration = configuration;

        this.loadDefault();
    }

    loadDefault(){
        this.changeBackground(this.configuration.default_background.texture);
    }

    setPlayerPokemon(pokemon){
        this.removePokemon('player_pokemon');

        pokemon.name = 'player_pokemon';
        pokemon.x = 230;
        pokemon.y = 500;

        pokemon.scale.set(2, 2);

        this.addChild(pokemon);
    }

    setOpponentPokemon(pokemon){
        this.removePokemon('opponent_pokemon');

        pokemon.name = 'opponent_pokemon';
        pokemon.x = 1255;
        pokemon.y = 115;

        this.addChild(pokemon);
    }

    removePokemon(type){
        for(let i = 0; i < this.children.length; i++){
            if(this.children[i].name == type){
                this.children.splice(i, 1);
                break;
            }
        }
    }

    changeBackground(texture){
        let background = this.children.find( c => c.name == 'background' );

        if(background == undefined){
            background = new PIXI.Sprite();
            background.name = 'background';
            background.x = 0;
            background.y = 0;

            this.addChild(background);
        }

        background.texture = texture;
    }

    drawCombat(delta){
        console.log(delta, "combat");
    }
}

export { CombatInterface, CombatPokemon }