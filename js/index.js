import { createAnimationFactory } from "./Animation.js";
import { Player, PlayerController } from "./Player.js";
import { createStaticMovementController } from "./StaticMovementController.js";
import { WorldMap } from "./WorldMap.js";
import { CombatInterface, CombatPokemon } from "./CombatInterface.js";
import { Application } from "./Application.js";

const STAGE_OPEN_WORLD = 'open_world';
const STAGE_BATTLE = 'battle';

var current_stage = undefined;

let app = new Application();

var world = undefined,
    movementController = undefined,
    combat = undefined,
    playerController = undefined;

function main(){
    document.body.appendChild(app.view);

    app.loader.load(init);
}

async function init(){
    createAnimationFactory(app.loader.resources['data/config/animations.json']);

    let worldConfig = app.loader.resources['data/config/routes.json'].data;
    let combatConfig = app.loader.resources['data/config/combat.json'].data;

    combatConfig.default_background.texture = app.loader.resources[
        combatConfig.default_background.path
    ].texture;

    for(let i = 0; i < worldConfig.routes.length; i++){
        worldConfig.routes[i].background.texture = app.loader.resources[
            worldConfig.routes[i].background.texture_path
        ].texture;

        worldConfig.routes[i].foreground.texture = app.loader.resources[
            worldConfig.routes[i].foreground.texture_path
        ].texture;
        
        for(let j = 0; j < worldConfig.routes[i].animatables.length; j++){
            if(worldConfig.routes[i].animatables[j].classname == 'Grass'){
                worldConfig.routes[i].animatables[j].textures = [
                    app.loader.resources['data/assets/grass/default.png'].texture,
                    app.loader.resources['data/assets/grass/lowered.png'].texture
                ];
            }else{
                worldConfig.routes[i].animatables[j].textures = [];
            }
        }
    }

    world = new WorldMap(worldConfig, app.layer.world);
    combat = new CombatInterface(combatConfig);

    combat.setPlayerPokemon( new CombatPokemon('raichu', 'player'));
    combat.setOpponentPokemon( new CombatPokemon('raichu'));

    app.stage.addChild(world);
    app.stage.addChild(combat);

    movementController = createStaticMovementController(world, world.getAnimatables(), world.getBlockers());
    playerController = new PlayerController(app);

    app.keyHandler.assign("1", "press", _ => { changeGamestate(STAGE_OPEN_WORLD); });
    app.keyHandler.assign("2", "press", _ => { changeGamestate(STAGE_BATTLE); });
    
    changeGamestate(STAGE_OPEN_WORLD);

    app.ticker.add(delta => gameLoop(delta));
}

function gameLoop(delta){
    if(current_stage == STAGE_OPEN_WORLD){
        movementController.move(delta);
        
    }else if(current_stage == STAGE_BATTLE){
        combat.drawCombat(delta);
    }
}

function changeGamestate(stage){
    if(stage == STAGE_OPEN_WORLD){
        combat.visible = false;
        world.visible = true;

    }else if(stage == STAGE_BATTLE){
        combat.visible = true;
        world.visible = false;

    }

    current_stage = stage;
}

// class WorldMap extends PIXI.Sprite {
//     constructor(texture = undefined){
//         super(texture);

//         this.tileset = { width : 0, height: 0 };
//         this.sortableChildren = true;

//         this.x = -6*128;
//         this.y = (9-40)*128;

//         this.parentLayer = layer;
//         this.zOrder = 0;

//         // this.loadTiles();
//     }

//     async loadTiles(){
//         let mappings = loader.resources['data/config/sourcemap.json'].data;
//         mappings = mappings.mappings;

//         for(let i = 0; i < mappings.length; i++){
//             if(mappings[i].tilePosition.x == -1 || mappings[i].tilePosition.y == -1){
//                 continue;
//             }

//             let texture = new PIXI.Texture(
//                 loader.resources['data/tileset_128x128.png'].texture,
//                 new PIXI.Rectangle(
//                     mappings[i].tilePosition.x,
//                     mappings[i].tilePosition.y,
//                     128, 128
//                 )
//             );

//             let sprite = new PIXI.Sprite(texture);

//             sprite.zIndex = 1;
                
//             sprite.x = mappings[i].mapPosition.x;
//             sprite.y = mappings[i].mapPosition.y;
            
//             sprite.positioning = {x: sprite.x, y: sprite.y};

//             this.addChild(sprite);
            
//             this.tileset.width = mappings[i].mapPosition.x + 128;
//             this.tileset.height = mappings[i].mapPosition.y + 128;
//         }

//     }
// }

document.addEventListener('DOMContentLoaded', main);