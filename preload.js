const { contextBridge } = require('electron')

contextBridge.exposeInMainWorld('myAPI', {  desktop: true})

// async function read(path){
//     return new Promise( resolve => {
//         fs.readFile(path, {encoding: 'utf-8'}, (err, data) => {
//             if(err){ throw err; }

//             resolve(data);
//         })
//     });
// }

window.addEventListener('DOMContentLoaded', async _ => {
    const replaceText = (selector, text) => { 
        const element = document.getElementById(selector)    
        if (element) element.innerText = text 
    }

    for (const dependency of ['chrome', 'node', 'electron']) { 
        replaceText(`${dependency}-version`, process.versions[dependency]) 
    }
});