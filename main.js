const { app, BrowserWindow } = require('electron');
const path = require('path')

function createWindow() {
    const win = new BrowserWindow({ 
        fullscreen: true,
        autoHideMenuBar: true,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js')
        }
    })
    
    win.webContents.openDevTools();

    win.loadFile('index.html')
}

app.commandLine.appendSwitch('disable-frame-rate-limit')

app.whenReady().then(() => {  createWindow()})

app.on('window-all-closed', _ => {  
    if (process.platform !== 'darwin') app.quit()
})