const Jimp = require('jimp');
const fs = require('fs');
const crypto = require('crypto');

const opacueHash = 'b8da25f00f6bc731b1d34d06a8d0660aeba95188483401b918a613327eb2a5dc';

let tileset = undefined;
let map = undefined;

let tiles = [];
let mapBlocks = [];

async function init(){
    tileset = await Jimp.read('data/tileset_128x128.png');
    map = await Jimp.read('data/route_1.png');

    // let compare = await Jimp.read('scripts/tiles/compare.png');

    await loadTiles();
    await loadMap();

    console.log("tiles: ", tiles.length);
    console.log("map-blocks: ", mapBlocks.length);

    let sourcemap = await readMap();

    console.log(sourcemap);

    fs.writeFile('scripts/sourcemap.json', JSON.stringify(sourcemap), err => {
        if(err){ throw err; }
    });
}

async function loadTiles(){
    let offset = 8;
    let block = { width: 128, height: 128 };
    let yMax = (tileset.bitmap.width-offset) / (block.height+offset);
    let xMax = (tileset.bitmap.height-offset) / (block.width+offset);
    let workingImage = undefined;
    let img = undefined;

    let hash = undefined
    let hashVal = '';
    
    for(let y = 1; y < yMax; y++){
        for(let x = 1; x < xMax; x++){
            hash = crypto.createHash('sha256');

            workingImage = tileset.clone();

            img = workingImage.crop(
                offset*x + block.width*(x-1),
                offset*y + block.height*(y-1),
                block.width,
                block.height
            );
            
            hash.update(JSON.stringify(img.bitmap.data));
            hashVal = hash.digest('hex');

            if(hashVal == opacueHash){
                continue;
            }

            tiles.push({
                position: {
                    x: offset*x + block.width*(x-1),
                    y: offset*y + block.height*(y-1)
                },
                image: img
            });

        }
    }
}

async function loadMap(){
    let block = { width: 128, height: 128 };
    let imageCopy = undefined;

    for(let y = 0; y < map.bitmap.height/block.height; y++){
        for(let x = 0; x < map.bitmap.width/block.width; x++){
            imageCopy = await map.clone();

            mapBlocks.push({
                position: {
                    x: block.width*x,
                    y: block.height*y,
                },
                image: imageCopy.crop(
                    block.width*x,
                    block.height*y,
                    block.width,
                    block.height
                )
            });
        }
    }
}

async function getTile(compareImage){
    let diff = undefined;

    for(let i = 0; i < tiles.length; i++){
        diff = Jimp.diff(tiles[i].image, compareImage);
            
        if(diff.percent == 1){ continue; }
        
        if(diff.percent == 0){
            return tiles[i].position;
        }
    }

    return {x: -1, y: -1};
}

async function readMap(){
    let mappings = [];

    for(let i = 0; i < mapBlocks.length; i++){
        mappings.push(
            {
                mapPosition: mapBlocks[i].position,
                tilePosition: await getTile(mapBlocks[i].image)
            }
        );
    }

    return {
        mappings: mappings
    };
}

init();