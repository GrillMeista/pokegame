const fs = require('fs');
const Jimp = require('jimp');

let tileset = map = undefined;

(async _ => {
    tileset = await Jimp.read('data/tileset_128x128.png');
    
    let mappings = await read('./scripts/sourcemap.json');
    mappings = JSON.parse(mappings);
    mappings = mappings.mappings;
    
    let width = mappings[mappings.length-1].mapPosition.x + 128;
    let height = mappings[mappings.length-1].mapPosition.y + 128;

    let finalMap = await createImage(width, height);


    for(let i = 0; i < mappings.length; i++){
        if(
            mappings[i].tilePosition.x < 0 ||
            mappings[i].tilePosition.y < 0
        ){
            continue;
        }

        let workingSet = await tileset.clone();

        finalMap.blit(
            workingSet.crop(
                    mappings[i].tilePosition.x, 
                    mappings[i].tilePosition.y, 
                    128, 128
                ), 
                mappings[i].mapPosition.x, 
                mappings[i].mapPosition.y
        );
    }

    await finalMap.writeAsync('scripts/map.png');

})();

async function read(path){
    return new Promise( resolve => {
        fs.readFile(path, {encoding: 'utf-8'}, (err, data) => {
            if(err){
                throw err;
            }

            resolve(data);
        })
    });
}

async function createImage(width, height){
    return new Promise( resolve => {
        new Jimp(width, height, 0xff0000ff, (err, img) => {
            if(err){ throw err; }

            resolve(img);
        });
    });
}