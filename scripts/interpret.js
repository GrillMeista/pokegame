const fs = require('fs');

async function read(path){
    return new Promise( resolve => {
        fs.readFile(path, {encoding: 'utf-8'}, (err, data) => {
            if(err){
                throw err;
            }

            resolve(data);
        })
    });
}

async function init(){
    let mappings = await read('./scripts/sourcemap.json');
        mappings = JSON.parse(mappings);
        mappings = mappings.mappings;

    console.log(mappings[0]);
}

init();